import React, { Component } from "react";
import { connect } from 'react-redux';
import { EDIT_SV, XOA_SV } from "./constant.js/constant";

class UserTable extends Component {
  renderUserList=()=>{
    let userArr = this.props.svArr.filter((value)=>{
      return value.hoVaTen.toUpperCase().includes(this.props.search.trim().toUpperCase())
    });

    return userArr.map((sv,index)=>{
      return(
        <tr key={index}>
          <td>{sv.maSv}</td>
          <td>{sv.hoVaTen}</td>
          <td>{sv.sdt}</td>
          <td>{sv.email}</td>
          <td>
            <button onClick={()=>{
              this.props.handleEdited(sv)
            }} className="btn btn-warning">Edit</button>
            <button onClick={()=>{
              this.props.handleRemoveSv(sv.maSv)
            }} className="btn btn-danger ml-2">Delete</button>
          </td>
        </tr>
      )
    })
  };

  render() {
    return (
      <table className="table mt-3 text-white">
        <thead>
          <tr className="bg-dark">
            <th>Mã SV</th>
            <th>Họ và tên</th>
            <th>Số điện thoại</th>
            <th>Email</th>
            <th>More</th>
          </tr>
        </thead>
        <tbody className="text-dark" >{this.renderUserList()}</tbody>
      </table>
    );
  }
};
let mapStateToProps=(state)=>{
  return {
    svArr:state.formReducer.svArr,
    search:state.formReducer.search,
  };
};
let mapdispatchToProps =(dispatch)=>{
  return{
    handleRemoveSv:(idSv)=>{
      dispatch({type:XOA_SV,payload:idSv});
    },
    handleEdited:(sv)=>{
      dispatch({type:EDIT_SV,payload:sv});
    }
  }
}
export default connect(mapStateToProps,mapdispatchToProps)(UserTable);
