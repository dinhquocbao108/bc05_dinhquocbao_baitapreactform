import React, { Component } from "react";
import { connect } from "react-redux";
import { THEM_SV, UPDATE_SV } from "./constant.js/constant";

class UserForm extends Component {
  constructor(props){
    super(props);
    this.inputRef = React.createRef();
  };
  state = {
    values: {
      maSv: "",
      hoVaTen: "",
      sdt: "",
      email: "",
    },
    errors: {
      maSv: "",
      hoVaTen: "",
      sdt: "",
      email: "",
    },
    valid: false,
    flag:false,
  };
  handleSubmit = () => {
    let index = this.props.user.findIndex((user)=>{
       return user.maSv*1===this.state.values.maSv*1;
    });
    if(index==-1){
      this.props.addNewSv(this.state.values);
    }
    else{
      alert("sinh viên đã tồn tại");
      return;
    }
    
  };
  handleUpdateSv=()=>{
    let index = this.props.user.findIndex((user)=>{
      return user.maSv*1===this.state.values.maSv*1;
   });
   if(index==-1){
    return;
   };
   if(index!=-1){
    let cloneUser = {...this.props.user,value:this.state.values};
    this.props.updateSv(cloneUser.value);
   }
    
    
  }
  handleChange = (e) => {
    let { value, name, type, pattern } = e.target;
    let errorMessage = "";
    // kiểm tra rỗng
    if (value.trim() === "") {
      errorMessage = name + "  không được để trống!";
    }
    // kiểm tra email
    if (type === "email") {
      let regex = new RegExp(pattern);
      if (!regex.test(value)) {
        errorMessage = name + " email không đúng định dạng!";
      }
    }
    // kiểm tra sdt
    if (name === "sdt") {
      let regex = new RegExp(pattern);
      if (!regex.test(value)) {
        errorMessage = name + " số điện thoại phải là số!";
      }
    }
    let values = { ...this.state.values, [name]: value };
    let errors = { ...this.state.errors, [name]: errorMessage };
    this.setState({ ...this.state, values: values, errors: errors }, () => {
      this.checkValid();
    });
  };

  checkValid = () => {
    let valid = true;
    for (let key in this.state.errors) {
      if (this.state.errors[key] !== "" || this.state.values[key] === "") {
        valid = false;
      }
      this.setState({ ...this.state, valid: valid });
    }
  };
  componentWillReceiveProps(nextProps){
    if(nextProps.userEdited!=null){
      this.setState({values:nextProps.userEdited,flag:true});
    }
  }

  render() {
    return (
      <div>
        <div
          className="bg-dark text-white d-flex justify-content-center align-items-center"
          style={{
            height: "40px",
          }}
        >
          Thông Tin Sinh Viên
        </div>
        <div className="row py-3">
          <div className="col-6">
            <div className="form-group">
              <span>Mã Sinh Viên</span>
              <input
              type="number"
                className="form-control"
                name="maSv"
                value={this.state.values.maSv}
                onChange={this.handleChange}
                ref={this.inputRef}
              />
              <p className="text-danger">{this.state.errors.maSv}</p>
            </div>
          </div>
          <div className="col-6">
            <div className="form-group">
              <span>Họ Và Tên</span>
              <input
                className="form-control"
                name="hoVaTen"
                value={this.state.values.hoVaTen}
                onChange={this.handleChange}
                ref={this.inputRef}
              />
              <p className="text-danger">{this.state.errors.hoVaTen}</p>
            </div>
          </div>
          <div className="col-6">
            <div className="form-group">
              <span>Số Điện Thoại</span>
              <input
                type="text"
                pattern="^\d+$"
                className="form-control"
                name="sdt"
                value={this.state.values.sdt}
                onChange={this.handleChange}
                ref={this.inputRef}
              />
              <p className="text-danger">{this.state.errors.sdt}</p>
            </div>
          </div>
          <div className="col-6">
            <div className="form-group">
              <span>Email</span>
              <input
                type="email"
                pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                className="form-control"
                name="email"
                value={this.state.values.email}
                onChange={this.handleChange}
                ref={this.inputRef}
              />
              <p className="text-danger">{this.state.errors.email}</p>
            </div>
          </div>
        </div>
        {this.state.valid ? (
          <button
            type="submit"
            onClick={this.handleSubmit}
            className="btn btn-success mr-3"
          >
            Thêm Sinh Viên
          </button>
        ) : (
          <button
            type="submit"
            disabled
            onClick={this.handleSubmit}
            className="btn btn-success mr-3 "
          >
            Thêm Sinh Viên
          </button>
        )}
        
       
        {this.state.flag?(<button onClick={this.handleUpdateSv} className="btn btn-primary">Update</button>):( <button className="btn btn-primary" disabled>Update</button>)}
      </div>
    );
  }
}
let mapStateToProps = (state)=>{
return{
  userEdited:state.formReducer.userEdited,
  user:state.formReducer.svArr,
};
};
let dispatchToProps = (dispatch) => {
  return {
    addNewSv: (sv) => {
      dispatch({ type: THEM_SV, payload: sv });
    },
    updateSv:(sv)=>{
      dispatch({type:UPDATE_SV,payload:sv});
    }
  };
};

export default connect(mapStateToProps, dispatchToProps)(UserForm);
