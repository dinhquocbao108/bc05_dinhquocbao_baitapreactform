import {
  EDIT_SV,
  GET_SEARCH,
  THEM_SV,
  UPDATE_SV,
  XOA_SV,
} from "./../../constant.js/constant";

const stateDefault = {
  svArr: [
    {
      maSv: 1,
      hoVaTen: "Đinh Quốc Bảo",
      sdt: "0909090909",
      email: "dinhquocbao108@gmail.com",
    },
    {
      maSv: 2,
      hoVaTen: "Nguyễn Văn A",
      sdt: "050505050",
      email: "dinhquobao108@gmail.com",
    },
    {
      maSv: 3,
      hoVaTen: "Trần Văn B",
      sdt: "07077070",
      email: "dinhquobao108@gmail.com",
    },
    {
      maSv: 4,
      hoVaTen: "Lê Văn c",
      sdt: "08080808",
      email: "dinquocbao108@gmail.com",
    },
  ],
  userEdited: null,
  search:'',
};
export const formReducer = (state = stateDefault, { type, payload }) => {
  switch (type) {
    case THEM_SV: {
      const newSv = [...state.svArr, payload];
      state.svArr = newSv;
      return { ...state };
    }
    case XOA_SV: {
      let index = state.svArr.findIndex((item) => {
        return item.maSv === payload;
      });
      if (index != -1) {
        let cloneUserList = [...state.svArr];
        cloneUserList.splice(index, 1);
        state.svArr = cloneUserList;
        return { ...state };
      }
    }
    case EDIT_SV: {
      state.userEdited = payload;
      return { ...state };
    }
    case UPDATE_SV: {
      let index = state.svArr.findIndex((item) => {
        return item.maSv === payload.maSv;
      });
      let cloneSvArr = [...state.svArr];
      cloneSvArr[index]=payload;
      return { ...state, svArr: cloneSvArr };
    };
    case GET_SEARCH:{
      return{...state,search:payload}
    };
    default:
      return state;
  }
};
