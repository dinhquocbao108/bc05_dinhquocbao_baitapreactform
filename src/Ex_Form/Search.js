import React, { Component } from "react";
import { connect } from "react-redux";
import { GET_SEARCH } from "./constant.js/constant";

class Search extends Component {
  constructor(props) {
    super(props);
    this.searchRef = React.createRef();
  }
  state = {
    search: "",
  };
  handleGetSearch = (e) => {
    let { value } = e.target;
    this.setState({ search: value });
    this.props.handleGetValueSearch(this.state.search);
  };

  render() {
    return (
      <div className="mt-3">
        <div className="form-group">
          <input
            value={this.state.search}
            ref={this.searchRef}
            onChange={this.handleGetSearch}
            type="text"
            className="form-control"
            name="search"
            placeholder="Tìm kiếm sinh viên theo tên"
          />
        </div>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handleGetValueSearch: (value) => {
      dispatch({ type: GET_SEARCH, payload: value });
    },
  };
};
export default connect(null, mapDispatchToProps)(Search);
