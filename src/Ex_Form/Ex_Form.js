import React, { Component } from 'react'
import Search from './Search'
import UserForm from './UserForm'
import UserTable from './UserTable'

export default class Ex_Form extends Component {
  render() {
    return (
      <div className='container'>
        <UserForm/>
        <Search/>
        <UserTable/>
      </div>
    )
  }
}
